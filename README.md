# Typescript

- 01 Simple types
- 02 Functions
- 03 Arrays
- 04 Complex types
- 05 Enums
- 06 Interfaces
- 07 Extends
- 08 Generic
- 09 Classes
- 09.1 Inheritance
- 09.2 Abstration
- 09.3 Static
- 09.4 Getters and Setters
- 10 Annotations
- 11 Promises
- 12 React
